package com.example.skiner.taskforprovectus.recycler;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.skiner.taskforprovectus.R;

public class ViewHolder extends RecyclerView.ViewHolder {

    private final TextView mValue;

    static ViewHolder newInstance(ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.item_recycler, parent, false);

        return new ViewHolder(view);
    }

    private ViewHolder(View view) {
        super(view);
        mValue = (TextView) view.findViewById(R.id.text);
    }

    void bind(String text) {
        mValue.setText(text);
    }
}