package com.example.skiner.taskforprovectus.recycler;

import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

public class Adapter extends RecyclerView.Adapter<ViewHolder> {

    private String[] mValue;

    public Adapter(String[] value) {
        mValue = value;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return ViewHolder.newInstance(parent);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bind(mValue[position]);
    }

    @Override
    public int getItemCount() {
        return mValue.length ;
    }
}