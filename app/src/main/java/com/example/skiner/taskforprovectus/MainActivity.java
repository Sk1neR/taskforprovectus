package com.example.skiner.taskforprovectus;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import com.example.skiner.taskforprovectus.recycler.Adapter;
import com.example.skiner.taskforprovectus.recycler.ItemDecoration;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
    }

    @Override
    public void onContentChanged() {
        super.onContentChanged();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Adapter adapter = new Adapter(fillingArray());

        RecyclerView recycler = (RecyclerView) findViewById(R.id.recycler_view_list);

        if (recycler != null) {
            recycler.setLayoutManager(new LinearLayoutManager(this));
            recycler.addItemDecoration(
                    new ItemDecoration(this, LinearLayoutManager.VERTICAL)
            );
            recycler.setAdapter(adapter);
        }
    }

    private String[] fillingArray() {
        String[] value = new String[101];

        for (int i = 0; i < value.length; i++) {
            value[i] = checkValue(i);
        }

        return value;
    }

    private String checkValue(int value) {
        if (value % 3 == 0) {
            return getString(R.string.multiple_of_three);
        }

        if (value % 5 == 0) {
            return getString(R.string.multiple_of_five);
        }

        if (value % 15 == 0) {
            return getString(R.string.multiple_of_fifteen);
        }

        return String.valueOf(value);
    }

}